import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Window {
    property int big_margin: 10
    property int small_margin: 5

    width: 480
    height: 360
    visible: true
    title: qsTr("Hello World")

    color: "#f2eae6"


    ColumnLayout {
        id: main_layout
        anchors.fill: parent
        spacing: 5
        anchors.margins: 10
        GroupBox {
            title: "Name"
            Layout.fillWidth: true
            RowLayout {
                id: name_input
                anchors.fill: parent
                TextField {
                    placeholderText: "Enter your name"
                }
                Button {
                    Layout.alignment: Qt.AlignRight
                    text: "Submit"
                }
            }
        }
        GroupBox {
            title: "Question"
            Layout.fillWidth: true
            Text {
                text: "Which is the superior animal?"
            }
        }

        GroupBox {
            title: "Answers"
            Layout.fillWidth: true
            Layout.fillHeight: true
            GridLayout {
                id: button_grid
                columns: 2
                rows: 2
                anchors.fill: parent

                Button {
                    // https://stackoverflow.com/questions/33086197/columnlayout-items-size-proportions
                    Layout.preferredWidth: parent.width / 2
                    Layout.preferredHeight: parent.height / 2
                    text: "Monkey";
                }
                Button {
                    Layout.preferredWidth: parent.width / 2
                    Layout.preferredHeight: parent.height / 2
                    text: "Human";
                }
                Button {
                    Layout.preferredWidth: parent.width / 2
                    Layout.preferredHeight: parent.height / 2
                    text: "Elephant";
                }
                Button {
                    Layout.preferredWidth: parent.width / 2
                    Layout.preferredHeight: parent.height / 2
                    text: "Zebra";
                }
            }
        }
    }
}
